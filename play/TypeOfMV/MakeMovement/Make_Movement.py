from play.Verifiers.CheckMate import king_in_check, king_in_checkmate
from play.Verifiers.CheckMate import mv_that_puts_their_own_k_in_ck,check_en_passant
from play.Verifiers.CheckRCastling import check_r_castling
from play.Verifiers.CheckKCastling import check_k_castling
from play.TypeOfMV.MakeMovement.messages import *

def make_movement(
        X1, Y1, X2, Y2,
        en_passant,
        chess_board, piece,
        king, SCA, LCA, prom_piece):

    """
    Return a tuple indicating the king turn, SCA, LCA and a message
    using a given king, The piece and his departure pos and
    destination pos in a chessboard and SCA, LCA

    LCA = Long castling availability also Queenside castling
    SCA = Short castling availability also Kingside castlin
    
    Y1, X1 -> Departure square
    Y2, X2 -> Destination square

    optional: en passant
    """
    
    en_passant[0], en_passant[1] = check_en_passant(
            X1, Y1, X2, Y2,
            en_passant, chess_board)
            # Modifying en_passant privilege
    # Making the movement
    chess_board[Y2][X2] = chess_board[Y1][X1]
    chess_board[Y1][X1] = '0'
   
    if piece == 'P' and prom_piece is not None:
        if king == 'K': chess_board[Y2][X2] = prom_piece
        elif king == 'k': chess_board[Y2][X2] = prom_piece.lower()

    elif piece == 'R':
            # A rook moved, revoke the castling privilege of SCA or LCA
            # Depending on the position of the rook

        SCA, LCA = check_r_castling(SCA, LCA, chess_board)

    elif piece == 'K':
            # A king moved, revoke the castling privilege of SCA and LCA
            # Because it's a king, so it's over!
        SCA, LCA = check_k_castling(king, SCA, LCA)

    # Changing king turn, because player made the movement 
    if king == 'k': king = 'K'
    elif king == 'K': king = 'k'

    # Checking if mv puts enemy king on check
    if king_in_check(
            king, chess_board,
            en_passant):
         
        # Checking if mv puts enemy king on checkmate
        if king_in_checkmate(
                king, chess_board,
                en_passant):
            
            if king == 'K':
                message = white_king_in_checkmate
                king = '0-1'

            elif king == 'k':
                message = black_king_in_checkmate
                king = '1-0'
        
        else:
            if king == 'K':
                message = white_king_in_check

            elif king == 'k':
                message = black_king_in_check
    
    else:
        # Checking for stalemate
        # Stalemate is checkmate without having to be in check
        if king_in_checkmate(
                'k', chess_board,
                en_passant):
            
            king = '1/2-1/2'
            message = stalemate
        
        elif king_in_checkmate(
                'K', chess_board,
                en_passant):
            
            king = '1/2-1/2'
            message = stalemate


        else:
            message = ""

    return king, SCA, LCA, message
