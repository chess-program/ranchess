# --------------- Wrong input -----------------------
DMVDreHasToBeUpper = "Departure piece has to be UPPER case letter"
DMVDionSQDoesntExist = "Destination square doesn't exist in the board"
DMVPieceInTheDionSQ = "Piece located in the destination square"
DMVPieceCantMVToSQ = "Your piece can't move to that square"
DMVItWillCauseCheck = "You can't move your piece to that square, It will cause check in your own king"
DMVDeparturePieceDoesntExist = "Departure piece doesn't exist"
DMVepPriv = "Your displacement move isn't allowed to capture in en passant"

# ----------------------------------------------------- #
# Valid capture input func messages

DMVHyphenNotRightPlace = "The '-' is not in the right place"
DMVContainMoreThan1Hyphen = "Your move contains more than 1 '-'"
DMVContainXandHyphen = "Your move can't be Capture and Displacement at the same time"
DMVHasToBeBetween6and9ch = "Your displacement move has to be between 6 and 9 characters"

DMVCheckDisabled = "CHDisabled"
DMVCheckActivated = "CHActivated"
DMVCheckmateActivated = "CHMActivated"

# ---------- P R O M O T I O N -----------#
DPromMVSignWrongPlace = "Your promotion mv doesn't contains the '=' sign in the right place"
DPromMVContainMoreThan1EQ = "Your promotion mv contains more than 1 '=' sign"

# ---------- E N . P A S S A N T ---------#
DEPMVSignWrongPlace = "Your en passant mv doesn't contain the 'e.p' sign in the right place"
DEPMVContainMoreThan1EP = "Your en passant mv contains more than 1 'e.p' sign"

# ---------- MV Equal 6 ch long ---------- #
DNormalMVContainIllegalSign = "Your normal mv 6 ch long contain illegal sign"

# ---------- MV Equal 7 ch long --------- #
DNormalchMVContainIllegalSign = "Your normal check mv 7 ch long contain illegal sign"
DNormalchMVSignWrongPlace = "Your normal check mv 7 ch long doesn't ends with '+' sign"
DNormalchMVContainMoreThan1CH = "Your normal check mv contain more than 1 '+' sign"

DNormalchmMVContainIllegalSign = "Your normal checkmate mv 7 ch long contain illegal sign"
DNormalchmMVSignWrongPlace = "Your normal checkmate mv 7 ch long doesn't ends with '#' sign"
DNormalchmMVContainMoreThan1CHM = "Your normal checkmate mv contain more than 1 '#' sign"

DMVeq7chDoesntContainCHorCHM = "Your mv is eq 7 ch long, but it doesn't contain '#' or '+' sign"

# ---------- MV Equal 8 ch long --------- #
DMVeq8chDoesntContainEQ = "Your mv is eq 8 ch long, but it doesn't contain '=' sign"
DPromMVContainIllegalSign = "Your promotion mv contains illegal sign"

# ---------- MV Equal 9 ch long --------- #
DMVeq9chDoesntContainEQ = "Your mv is eq 9 ch long, but it doesn't contain '=' sign"
DPromMVDoesntContainCHorCHM = "Your promotion mv is eq 9 ch long, but it doesn't contain '#' or '+' sign"
DPromchMVContainIllegalSign = "Your promotion check mv 9 ch long contain illegal sign"
DPromchMVSignWrongPlace = "Your promotion check mv 9 ch long doesn't ends with '+' sign"
DPromchMVContainMoreThan1CH = "Your promotion check mv contain more than 1 '+' sign"

DPromchmMVContainIllegalSign = "Your promotion checkmate mv 9 ch long contain illegal sign"
DPromchmMVSignWrongPlace = "Your promotion checkmate mv 9 ch long doesn't ends with '#' sign"
DPromchmMVContainMoreThan1CHM = "Your promotion checkmate mv contain more than 1 '#' sign"
