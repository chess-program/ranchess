from play.TypeOfMV.LegitDeparture.messages import *
from play.models import rows, columns

def legit_departure(
        piece, x, y,
        player_turn, chess_board):

    """
    Return a bool if it's a legit departure if not return a message
    using the given piece input, his position in a chessboard and
    the player turn
    """

    if y in rows.keys() and x in columns.keys():
            # Y is a str(number) and belongs to the range 1-8
            # X is a str(letter) and belongs to the range a-h
        attacking_piece = chess_board[rows.get(y)][columns.get(x)]
        if piece == attacking_piece.upper():
                # If the input piece is the same piece as the piece in SQ Y,X
            
            if (attacking_piece.isupper() and player_turn.isupper() or
                attacking_piece.islower() and player_turn.islower()):
                    # The piece belongs to the player
                return True

            else:
                return DrePieceBelongsToTheEnemyPlayer
        else:
            return DrePieceIsNotLocatedInTheSQ
    else:
        return DreSquareDoesntExistInTheBoard
