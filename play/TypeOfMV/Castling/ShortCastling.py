import copy
from play.Verifiers.CheckMate import king_in_check, king_in_checkmate
from play.Verifiers.CheckMate import mv_that_puts_their_own_k_in_ck,check_en_passant
from play.TypeOfMV.Castling.messages import *

def short_castling(
        king, chess_board,
        SCA, LCA, en_passant=None):
    
    """
    Return a tuple indicating the king turn, SCA and LCA availability
    and a message
    Using a given king, in a given chess_board and LCA, SCA

    LCA = Long castling availability also Queenside castling
    SCA = Short castling availability also Kingside castling

    optional: en passant
    """
    
    if king == 'K': Y = -1 # If it's white king check last row
    elif king == 'k': Y = 0 # If it's black king check first row
    
    if SCA.count(king) == 1:
            # Neither the king nor the rook has previously moved.
        if chess_board[Y][5] == '0' and chess_board[Y][6] == '0':
                # There are no pieces between the king and the rook

            if not king_in_check(king, chess_board, en_passant):
                    # King is not currently in check
                testing_board = copy.deepcopy(chess_board)
                testing_board[Y][5] = king
                testing_board[Y][4] = '0'
                # King moving 1 step to the right

                if not king_in_check(king, testing_board, en_passant):
                        # King moved 1 step to the right and didn't get ch
                    testing_board[Y][6] = king
                    testing_board[Y][5] = '0'
                    # King moving 2 step to the right

                    if not king_in_check(
                            king, testing_board, en_passant):
                            # King moved 2 steps to the right and didn't get ch

                        # Castling is available, making the movement
                        chess_board[Y][4] = '0'
                        chess_board[Y][6] = king
                        chess_board[Y][7] = '0'
                        if king == "K": chess_board[Y][5] = 'R'
                        elif king == 'k': chess_board[Y][5] = 'r'
                        
                        # The privilege has been taken, eliminate LCA and SCA
                        LCA = LCA.replace(king,'')
                        SCA = SCA.replace(king,'')

                        # Changing king turn, because player made the movement
                        if king == 'k': king = 'K'
                        elif king == 'K': king = 'k'

                        # revoking e.p privilege because player didn't take it
                        if en_passant[0] == True:
                            en_passant[0] = False
                            en_passant[1] = [-1, -1]
 
                        # Checking if mv puts enemy king on check
                        if king_in_check(
                                king, chess_board,
                                en_passant):
                            
                            # Checking if mv puts enemy king on checkmate
                            if king_in_checkmate(
                                    king, chess_board,
                                    en_passant):
                                
                                if king == 'K':
                                    king = '0-1'
                                    message = white_king_in_checkmate

                                elif king == 'k':
                                    king = '1-0'
                                    message = black_king_in_checkmate
                            
                            else: # King in check not checkmate
                                if king == 'k':
                                    message = black_king_in_check

                                elif king == 'K':
                                    message = white_king_in_check
                        
                        else: # King not in check

                            if king_in_checkmate(
                                    'k', chess_board,
                                    en_passant):
                                
                                king = '1/2-1/2'
                                message = stalemate
                            
                            elif king_in_checkmate(
                                    'K', chess_board,
                                    en_passant):
                                
                                king = '1/2-1/2'
                                message = stalemate
                            
                            # Stalemate is checkmate without being in check

                            else:
                                # Normal move
                                message = ""
                    else:
                        message = KingPassThroughSQInCheck
                else:
                    message = KingPassThroughSQInCheck
            else:
                message = KingCurrentlyInCheck
        else:
            message =  PiecesBetweenKandR
    else:
        message = SCNotAvailable

    return king, LCA, SCA, message
