import copy
from play.Verifiers.CheckMate import king_in_check, king_in_checkmate
from play.Verifiers.CheckMate import mv_that_puts_their_own_k_in_ck,check_en_passant
from play.TypeOfMV.Castling.messages import *

def long_castling(
        king, chess_board,
        LCA, SCA, en_passant=None):
    
    """
    Return a tuple indicating the king turn, SCA and LCA availability
    and a message
    using a given king, in a given chess_board and LCA, SCA

    LCA = Long castling availability also Queenside castling
    SCA = Short castling availability also Kingside castlin

    optional: en passant
    """
    
    if king == 'K': Y = -1 # If it's white king check last row
    elif king == 'k': Y = 0 # If it's black king check first row

    if LCA.count(king) == 1:
            # Neither the king nor the rook has previously moved.
        if (chess_board[Y][1] == '0' and
            chess_board[Y][2] == '0' and
            chess_board[Y][3] == '0'):
                # There are no pieces between the king and the rook

            if not king_in_check(king, chess_board, en_passant):
                    # If king is not currently in check
                testing_board = copy.deepcopy(chess_board)
                testing_board[Y][3] = king
                testing_board[Y][4] = '0'
                # King moving 1 step to the left

                if not king_in_check(king, testing_board, en_passant):
                        # King moved 1 step to the left and didn't get ch
                    testing_board[Y][2] = king
                    testing_board[Y][3] = '0'
                    # King moving 2 step to the left

                    if not king_in_check(
                            king, testing_board, en_passant):
                            # King moved 2 steps to the left and didn't get ch

                        # Castling is Available, making the movement
                        chess_board[Y][0] = '0'
                        chess_board[Y][2] = king
                        chess_board[Y][4] = '0'
                        if king == "K": chess_board[Y][3] = 'R'
                        elif king == 'k': chess_board[Y][3] = 'r'

                        # The privilege has been taken, eliminate LCA and SCA
                        LCA = LCA.replace(king,'')
                        SCA = SCA.replace(king,'')
                        
                        # Changing king turn, because player made the movement
                        if king == 'k': king = 'K'
                        elif king == 'K': king = 'k'
                        
                        # revoking e.p privilege because player didn't take it
                        if en_passant[0] == True:
                            en_passant[0] = False
                            en_passant[1] = [-1, -1]
                        
                        # Checking if mv puts enemy king on check
                        if king_in_check(
                                king, chess_board,
                                en_passant):
                            
                            # Checking if mv puts enemy king on checkmate
                            if king_in_checkmate(
                                    king, chess_board,
                                    en_passant):
                                
                                if king == 'K':
                                    message = white_king_in_checkmate
                                    king = '0-1'

                                elif king == 'k':
                                    message = black_king_in_checkmate
                                    king = '1-0'
                            
                            else: # King in check not checkmate
                                if king == 'k':
                                    message = black_king_in_check

                                elif king == 'K':
                                    message = white_king_in_check

                        else: # King not in check

                            if king_in_checkmate(
                                    'k', chess_board,
                                    en_passant):
                                
                                king = '1/2-1/2'
                                message = stalemate
                            
                            elif king_in_checkmate(
                                    'K', chess_board,
                                    en_passant):
                                
                                king = '1/2-1/2'
                                message = stalemate
                            
                            # Stalemate is checkmate without being in check

                            else:
                                # Normal move
                                message = ""
                    else:
                        message = KingPassThroughSQInCheck
                else:
                    message = KingPassThroughSQInCheck
            else:
                message = KingCurrentlyInCheck
        else:
            message =  PiecesBetweenKandR
    else:
        message = LCNotAvailable

    return king, LCA, SCA, message
