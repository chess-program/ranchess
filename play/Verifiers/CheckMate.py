import copy
from play.PieceMVS.ChooseMovement import choose_movements

def king_in_check(
        king, chess_board, en_passant):
    
    """
    Return a bool indicating if king is in check or not
    using the given king to check and a given chess_board

    optional: en_passant
    """

    king_location = [[X, Y] for Y, row in enumerate(chess_board)\
            for X, piece in enumerate(row) if piece == king][0]
        # Location of the given king

    for Y, row in enumerate(chess_board):
        for X, piece in enumerate(row):
                # Searching for enemies pieces

            if (piece.isupper() and king.islower() or
                piece.islower() and king.isupper()):
                    # If it's a enemy piece

                piece_mvs = choose_movements(X, Y, chess_board, en_passant.copy())
                    # Find the movements of that given enemy piece
                
                if king_location in piece_mvs:
                        # If the enemy piece mv reach king then
                    return True # King in check

    return False # King not in check

def king_in_checkmate(
        king, chess_board, en_passant):
    
    """
    Return a bool indicating if king is in checkmate or not
    using the given king to check and a given chess_board

    optional: en_passant
    """

    for Y1, row in enumerate(chess_board):
        for X1, piece in enumerate(row):
            # Searching for friend pieces
            
            if (piece.isupper() and king.isupper() or
                piece.islower() and king.islower()):
                    # If it's a friend piece

                testing_en_passant = en_passant.copy()
                piece_mvs = choose_movements(
                        X1, Y1, chess_board,
                        testing_en_passant)
                    # Find the movements of that given piece
                        
                for mv in piece_mvs:
                    if not mv_that_puts_their_own_k_in_ck(
                            king, mv, chess_board,
                            X1, Y1, en_passant):
                                # MV puts own king in safe
                
                        return False # King not in checkmate

    # After doing this process if there's not mv that put the king in safety
    return True # King in checkmate

def mv_that_puts_their_own_k_in_ck(
        king, move, chess_board,
        X1, Y1, en_passant):
    
    """
    Return a bool indicating whether or not the MV puts their own king in check
    Using a king, the move of the piece and his given position in a chess board
    
    Optional -> en passant
    """

    X2, Y2 = move
    testing_board = copy.deepcopy(chess_board)
    testing_en_passant = en_passant.copy()

    check_en_passant(X1, Y1, X2, Y2, testing_en_passant, testing_board)
    
    testing_board[Y2][X2] = chess_board[Y1][X1]
    testing_board[Y1][X1] = '0'
    
    if king_in_check(king, testing_board, testing_en_passant):
        return True # Illegal move, It will cause check in your king

    return False # Legal Move, It won't cause check in your king

def check_en_passant(
        X1, Y1, X2, Y2,
        en_passant, chess_board):
    
    """
    Return a modified e.p indicating if it's available or not
    using the position of the Piece and the position of his destination
    and his e.p availability, in a chess board.
    """

    piece = chess_board[Y1][X1]
    en_passant_exist, en_passant_piece = en_passant
    X3, Y3 = en_passant_piece

    if en_passant_exist: # If the privilege is available
        if (piece.upper() == 'P' and [X1, Y1 - 2] == [X2, Y2] or 
            piece.upper() == 'P' and [X1, Y1 + 2] == [X2, Y2]):
                # If the piece is a pawn and he's in his initial position
                # Moving two squares
            en_passant[0] = True
            en_passant[1] = [X2, Y2]
        
        elif [X3 - 1, Y3] == [X1, Y1] or [X3 + 1, Y3] == [X1, Y1]:
                # If the piece is in the left or right of e.p piece
            
            if piece == 'P' or piece == 'p':
                    # If it's a pawn

                if ([X3, Y3 - 1] == [X2, Y2] and piece.isupper() or
                    [X3, Y3 + 1] == [X2, Y2] and piece.islower()):
                        # If pawn is going to the e.p capture square
                    chess_board[Y3][X3] = '0'
                    en_passant[0] = False
                    en_passant[1] = [-1, -1]
                
                else: # It's not going to the e.p capture square
                    en_passant[0] = False
                    en_passant[1] = [-1, -1]

            else:
                # It's not pawn, so it's not going to take the privilege of e.p
                en_passant[0] = False
                en_passant[1] = [-1, -1]

        else: # It's a piece moving accross the board or attacking diff. piece
            en_passant[0] = False
            en_passant[1] = [-1, -1]
    
    else:
        if (piece.upper() == 'P' and [X1, Y1 - 2] == [X2, Y2] or 
            piece.upper() == 'P' and [X1, Y1 + 2] == [X2, Y2]):
                # If the piece is a pawn and he's in his initial position
                # Moving two squares
            en_passant[0] = True
            en_passant[1] = [X2, Y2]

    return en_passant # Return the modified e.p
