def check_k_castling(king, SCA, LCA):
    """
    Return a modified SCA and LCA indicating castling avaialbility
    Short castling and long castling respectively

    using the king that moved
    """

    LCA = LCA.replace(king,'')
    SCA = SCA.replace(king,'')

    return SCA, LCA
