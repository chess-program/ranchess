def rook_movements(X1, Y1, chess_board):
    """
    Return a list with all possible legal moves of the rook
    using his given position (X1, Y1) in a given chess_Board
    """

    attacking_piece = chess_board[Y1][X1]

    positive_vertical_line = []
    negative_vertical_line = []
    positive_horizontal_line = []
    negative_horizontal_line = []
   
    for Y2 in range(len(chess_board)):
        if Y2 < Y1:
                # It's positive_vertical_line (coming from +inf)
            if chess_board[Y2][X1] != '0':
                    # Square has piece on it
                
                target_piece = chess_board[Y2][X1]
                if (target_piece.islower() and attacking_piece.isupper() or
                    target_piece.isupper() and attacking_piece.islower()):
                        # has enemy on it
                    positive_vertical_line.clear()
                    positive_vertical_line.append([X1, Y2])

                else: # Piece from the player itself 
                    positive_vertical_line.clear()

            else: # It doesn't contains a piece
                positive_vertical_line.append([X1, Y2])

        elif Y2 > Y1:
                # It's negative_vertical_line (coming from rook int. pos)
            if chess_board[Y2][X1] != '0':
                    # Square has piece on it

                target_piece = chess_board[Y2][X1]
                if (target_piece.islower() and attacking_piece.isupper() or
                    target_piece.isupper() and attacking_piece.islower()):
                        # has enemy on it
                    negative_vertical_line.append([X1, Y2])
                    break
                
                else: # Piece from the player itself
                    break 

            else: # It doesn't contains a piece
               negative_vertical_line.append([X1, Y2])

    vertical_line = positive_vertical_line + negative_vertical_line

    for X2 in range(len(chess_board)):
        if X2 < X1:
                # It's negative_horizontal_line (coming from -inf)
            if chess_board[Y1][X2] != '0':
                    # Square has piece on it 
                target_piece = chess_board[Y1][X2]
                
                if (target_piece.islower() and attacking_piece.isupper() or
                    target_piece.isupper() and attacking_piece.islower()):
                        # has enemy on it
                    negative_horizontal_line.clear()
                    negative_horizontal_line.append([X2, Y1])
                
                else: # Piece from the player itself
                    negative_horizontal_line.clear()

            else: # It doesn't contains a piece
                negative_horizontal_line.append([X2, Y1])

        elif X2 > X1:
                # It's positive_horizontal_line (coming from rook int. pos)
            if chess_board[Y1][X2] != '0':
                    # Square has piece on it
                
                target_piece = chess_board[Y1][X2]
                if (target_piece.islower() and attacking_piece.isupper() or
                    target_piece.isupper() and attacking_piece.islower()):
                        # has enemy on it
                    positive_horizontal_line.append([X2, Y1])
                    break

                else: # Piece from the player itself
                    break

            else: # It doesn't contains a piece
                positive_horizontal_line.append([X2, Y1])

    horizontal_line = positive_horizontal_line + negative_horizontal_line
    
    return vertical_line + horizontal_line
