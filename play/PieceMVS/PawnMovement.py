def pawn_movements(X1, Y1, chess_board, en_passant):
    """
    Return a list with all possible legal moves of the pawn
    using his given position (X1, Y1) in a given chess_board
    
    Optional: En_passant
    """
    
    attacking_piece = chess_board[Y1][X1]
    
    moves = []
    
    if attacking_piece.isupper(): # White piece in the board
        if (Y1 - 1) >= 0: # Advance 1 Step up Available
            if (X1 - 1) >= 0: # Move left (capture) available
                if (chess_board[Y1 - 1][X1 - 1] != '0' and
                    chess_board[Y1 - 1][X1 - 1].islower()):
                        # If square is not empty and has enemy on it
                    moves.append([X1 - 1, Y1 - 1])

                else:
                    en_passant_exist, en_passant_piece = en_passant
                    X2, Y2 = en_passant_piece
                    if en_passant_exist:
                        if [X2 + 1, Y2] == [X1, Y1]:
                                # e.p Piece is in the left of attacking piece
                            moves.append([X1 - 1, Y1 - 1])
            
            if (X1 + 1) < len(chess_board): # Move right (capture) available
                if (chess_board[Y1 - 1][X1 + 1] != '0' and 
                    chess_board[Y1 - 1][X1 + 1].islower()):
                        # If square is not empty and has enemy on it
                    moves.append([X1 + 1, Y1 - 1])
                
                else:
                    en_passant_exist, en_passant_piece = en_passant
                    X2, Y2 = en_passant_piece
                    if en_passant_exist:
                        if [X2 - 1, Y2] == [X1, Y1]:
                                # e.p Piece is in the right of attacking piece
                            moves.append([X1 + 1, Y1 - 1])

            if chess_board[Y1 - 1][X1] == '0':
                    # Square 1 step up empty (displacement)
                moves.append([X1, Y1 - 1])
                
                if (Y1 == len(chess_board) - 2 and
                    chess_board[Y1 - 2][X1] == '0'):
                        # Square 2 step up empty and pawn is in pos initial

                    moves.append([X1, Y1 - 2])
                    en_passant[0] = True
                    en_passant[1] = [X1, Y1 - 2]


    elif attacking_piece.islower(): # Black piece
        if (Y1 + 1) < len(chess_board): # Advance 1 Step down Available
            if (X1 - 1) >= 0: # Move left (capture) available
                if (chess_board[Y1 + 1][X1 - 1] != '0' and
                    chess_board[Y1 + 1][X1 - 1].isupper()):
                            # If square is not empty and has enemy on it
                    moves.append([X1 - 1, Y1 + 1])
                
                else:
                    en_passant_exist, en_passant_piece = en_passant
                    X2, Y2 = en_passant_piece
                    if en_passant_exist:
                        if [X2 + 1, Y2] == [X1, Y1]:
                                # e.p Piece is in the left of attacking piece
                            moves.append([X1 - 1, Y1 + 1])

            if (X1 + 1) < len(chess_board): # Move right (capture) available
                if (chess_board[Y1 + 1][X1 + 1] != '0' and
                    chess_board[Y1 + 1][X1 + 1].isupper()):
                        # If square is not empty and has enemy on it
                    moves.append([X1 + 1, Y1 + 1])
                
                else:
                    en_passant_exist, en_passant_piece = en_passant
                    X2, Y2 = en_passant_piece
                    if en_passant_exist:
                        if [X2 - 1, Y2] == [X1, Y1]:
                                # e.p Piece is in the right of attacking piece
                            moves.append([X1 + 1, Y1 + 1])
 
            if chess_board[Y1 + 1][X1] == '0':
                    # Square 1 step down empty (displacement)
                moves.append([X1, Y1 + 1])
                
                if Y1 == 1 and chess_board[Y1 + 2][X1] == '0':
                        # Pawn is in pos initial and square 2 step down empty
                    moves.append([X1, Y1 + 2])
                    en_passant[0] = True
                    en_passant[1] = [X1, Y1 + 2]
    return moves
