def bishop_movements(X, Y, chess_board):
    """
    Return a list with all possible legal moves of the bishop
    using his given position (X, Y) in a given chess_board
    """

    attacking_piece = chess_board[Y][X] 
    
    half_negative_slope_up = []
    half_negative_slope_down = [] 
    half_positive_slope_down = []
    half_positive_slope_up = []
    
    # Half negative slope that goes up
    for Z in range(1, len(chess_board)):
        if  X - Z >= 0 and Y - Z >= 0: 
                # If advance 1 step up and 1 step left doesn't get you out
            if chess_board[Y - Z][X - Z] != '0':
                    # Square has piece on it

                target_piece = chess_board[Y - Z][X - Z]
                if (target_piece.islower() and attacking_piece.isupper() or
                    target_piece.isupper() and attacking_piece.islower()):
                        # has enemy on it
                    half_negative_slope_up.append([X - Z, Y - Z])
                    break

                else: 
                    break # Piece from the player itself

            else: # Square empty
                half_negative_slope_up.append([X - Z, Y - Z])
    
    # Half negative slope that goes down
    for Z in range(1, len(chess_board)):
        if  X + Z < len(chess_board) and Y + Z < len(chess_board):
                # If advance 1 step down and 1 step right doesn't get you out
            if chess_board[Y + Z][X + Z] != '0':
                    # Square has piece on it

                target_piece = chess_board[Y + Z][X + Z]
                if (target_piece.islower() and attacking_piece.isupper() or
                    target_piece.isupper() and attacking_piece.islower()):
                        # has enemy on it
                    half_negative_slope_down.append([X + Z, Y + Z])
                    break

                else: # Piece from the player itself
                    break

            else: # Square empty
                half_negative_slope_down.append([X + Z, Y + Z])
    
    # Half positive slope that goes down
    for Z in range(1, len(chess_board)):
        if  X - Z >= 0 and Y + Z < len(chess_board):
                # If advance 1 Step down and 1 step left doesn't get you out
            if chess_board[Y + Z][X - Z] != '0':
                    # Square has piece on it

                target_piece = chess_board[Y + Z][X - Z]
                if (target_piece.islower() and attacking_piece.isupper() or
                    target_piece.isupper() and attacking_piece.islower()):
                        # has enemy on it
                    half_positive_slope_down.append([X - Z, Y + Z])
                    break

                else: # Piece from the player itself
                    break

            else: # Square empty
                half_positive_slope_down.append([X - Z, Y + Z])
    
    # Half positive slope that goes up
    for Z in range(1, len(chess_board)):
        if  X + Z < len(chess_board) and Y - Z >= 0: 
                # If advance 1 Step up and 1 Step right doesn't get you out
            if chess_board[Y - Z][X + Z] != '0':
                    # Square has piece on it
                target_piece = chess_board[Y - Z][X + Z]
                if (target_piece.islower() and attacking_piece.isupper() or
                    target_piece.isupper() and attacking_piece.islower()):
                        # has enemy on it
                    half_positive_slope_up.append([X + Z, Y - Z])
                    break

                else: # Piece from the player itself
                    break

            else: # Square empty
                half_positive_slope_up.append([X + Z, Y - Z])
   
    negative_slope = half_negative_slope_up + half_negative_slope_down
    positive_slope = half_positive_slope_up + half_positive_slope_down
    slope = positive_slope + negative_slope
    
    return slope
