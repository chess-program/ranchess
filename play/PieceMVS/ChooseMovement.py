from .PawnMovement import pawn_movements
from .RookMovement import rook_movements
from .KnightMovement import knight_movements
from .BishopMovement import bishop_movements
from .QueenMovement import queen_movements
from .KingMovement import king_movements

def choose_movements(x, y, chess_board, en_passant):
    attacking_piece = chess_board[y][x]
    
    if attacking_piece == 'P' or attacking_piece == 'p':
        return pawn_movements(x, y, chess_board, en_passant)

    elif attacking_piece == 'R' or attacking_piece == 'r':
        return rook_movements(x, y, chess_board)
    
    elif attacking_piece == 'N' or attacking_piece == 'n':
        return knight_movements(x, y, chess_board)
    
    elif attacking_piece == 'B' or attacking_piece == 'b':
        return bishop_movements(x, y, chess_board)

    elif attacking_piece == 'Q' or attacking_piece == 'q':
        return queen_movements(x, y, chess_board)

    elif attacking_piece == 'K' or attacking_piece == 'k':
        return king_movements(x, y, chess_board)

    else:
        pass # .__.
