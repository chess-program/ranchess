from .RookMovement import *
from .BishopMovement import *

def queen_movements(X, Y, chess_board):
    """
    Return a list with all possible legal moves of the queen
    using his given position (X, Y) in a given chess board
    """

    rook_mvs = rook_movements(X, Y, chess_board)
    bishop_mvs = bishop_movements(X, Y, chess_board)
    queen_mvs = rook_mvs + bishop_mvs # No more explanation

    return queen_mvs
