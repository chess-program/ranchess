import os
from Graphics.CenterText import center_text

def print_game_notation(game_notation):
    games = ""
    temp = ""
    ch_limit = os.get_terminal_size()[0]
    print("")
    print(center_text(ch_limit, "Notation"))
    for play, notation in game_notation.items():
        if len(notation) == 2:
            W,B = notation[0], notation[1]
            temp += str(play) + '. ' + W + ' ' + B + ' '
        
        else:
            W = notation[0]
            temp += str(play) + '. ' + W + ' '

        if len(temp.strip()) == 33:
            print(center_text(ch_limit, temp.strip()))
            if len(notation) == 2:
                W,B = notation[0], notation[1]
                temp = str(play) + '. ' + W + ' ' + B + ' '
        
            else:
                W = notation[0]
                temp = str(play) + '. ' + W + ' '

            games = ""
            
        elif len(temp.strip()) > 33:
            print(center_text(ch_limit, games.strip()))
            temp = ""
            games = ""
        
        games = temp

    if games != "":
        print(center_text(ch_limit, games.strip()))

    print("")
