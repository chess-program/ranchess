import os
from Graphics.CenterText import center_text

def wrong_move(move, message, king_turn):
    ch_limit = os.get_terminal_size()[0]
    if len(move) > 15:
        if king_turn == "K":
            print(center_text(ch_limit, "Wrong move --> " + "'" + 
                                    move[0:13] + '...' + "'" + " (White)"))
        
        else:
            print(center_text(ch_limit, "Wrong move --> " + "'" + 
                                    move[0:13] + '...' + "'" + " (Black)"))

    else:
        if king_turn == "K":
            print(center_text(ch_limit, "Wrong move --> " + "'" + 
                                    move + "'" + " (White)"))
        
        else:
            print(center_text(ch_limit, "Wrong move --> " + "'" + 
                                    move + "'" + " (Black)"))
    
    temp = ""
    words = ""
    for word in message.split():
        temp += word + ' '

        if len(temp.strip()) == 33:
            print(center_text(ch_limit, temp.strip()))
            temp = ""
            words = ""
            
        elif len(temp.strip()) > 33:
            print(center_text(ch_limit, words.strip()))
            temp = word + ' '
            words = ""
        
        words = temp

    if words != "":
        print(center_text(ch_limit, words.strip()))
