import os
from .models import *
from .CenterText import center_text

def gen_graphics(chess_board):
    RTX_MAP = []
    RTX_ROW = ""
    ch_limit = os.get_terminal_size()[0]
    white_square_turn = True
    white_row_turn = True
    n = 8
    for row in chess_board:
        if white_row_turn:
            for piece in row:
                if white_square_turn:
                    if piece != '0':
                        RTX_ROW += (white_square.replace('x', piece))
                    
                    else:
                        RTX_ROW += (white_square.replace('x',' '))
                    
                    white_square_turn = False
                
                else: # Black square turn
                    if piece != '0':
                        RTX_ROW += (black_square.replace('x', piece))

                    else:
                        RTX_ROW += (black_square.replace('x',':'))

                    white_square_turn = True
            
            white_row_turn = False
            white_square_turn = False

        else:
            for piece in row:
                if white_square_turn:
                    if piece != '0':
                        RTX_ROW += (white_square.replace('x', piece))
                    
                    else:
                        RTX_ROW += (white_square.replace('x',' '))
                    
                    white_square_turn = False
                
                else: # Black square turn
                    if piece != '0':
                        RTX_ROW += (black_square.replace('x', piece))

                    else:
                        RTX_ROW += (black_square.replace('x',':'))

                    white_square_turn = True
            
            white_row_turn = True
            white_square_turn = True
        

        RTX_ROW = str(n) + ' ' + RTX_ROW + '| ' + str(n)
        RTX_MAP.append(center_text(ch_limit,
                            '  ' + '|===' * len(chess_board) + '|  '))
        RTX_MAP.append(center_text(ch_limit, RTX_ROW))
        RTX_ROW = ""

        n -= 1
    RTX_MAP.insert(0, center_text(ch_limit,
                        '    a   b   c   d   e   f   g   h    '))
    RTX_MAP.append(center_text(ch_limit,
                        '  ' + '|===' * len(chess_board) + '|  '))
    RTX_MAP.append(center_text(ch_limit,
                        '    a   b   c   d   e   f   g   h    '))
    return RTX_MAP

def print_new_graphics(new_board): 
    for row in new_board:
        for piece in row: 
            print(piece, end="")
        
        print("")
